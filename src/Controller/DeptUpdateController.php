<?php

namespace Drupal\dept_update\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dept_update\Client\Credentials;
use Drupal\update\Controller\UpdateController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeptUpdateController extends ControllerBase
{
    /**
     * @var DeptUpdateController
     */
    private $updateController;
    private $credentials;

    /**
     * UpdateEndpointController constructor.
     * @param UpdateController $updateController
     * @param Credentials $credentials
     */
    public function __construct(UpdateController $updateController, Credentials $credentials)
    {
        $this->updateController = $updateController;
        $this->credentials = $credentials;
    }

    /**
     * @param ContainerInterface $container
     * @return ControllerBase|DeptUpdateController
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('dept_update.drupal.update.controller'),
            $container->get('dept_update.client.credentials'));
    }

    /**
     * @param Request $request
     * @param $token
     * @return JsonResponse
     */
    public function index(Request $request, $token)
    {
        //is not production environment, first update the update status report
        if (!$this->credentials->isProd()) {
            $this->updateController->updateStatusManually();
        }

        if ($token === $this->credentials->getToken()) {
            return new JsonResponse([
                    'php-version' => phpversion(),
                    $this->updateController->updateStatus()]
            );
        } else {
            return new JsonResponse([
                'error' => ['access denied']
            ]);
        }
    }
}