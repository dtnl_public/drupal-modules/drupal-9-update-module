#dept_update
###Description
The dept_update module creates an endpoint for getting the update report and php version of the server in json format.
###Installation
1. Add our satis server as a repository in the composer.json file, if the satis repository is not exist in the drupal 8 project:
`{"type": "composer","url": "https://satis.tamtam.nl}`
2. Install dept_update module with the following composer command:
`composer require drupal/dept_update:1.0`
3. Add a token for endpoint as a settings variable in the settings.local.php:
`$settings['dept_update.token'] = 'ADD_SOME_REALLY_RAMDOM_TOKEN';`
4. Add is_prod as a settings variable in the settings.local.php, if the drupal site is not production the dept_update 
module update first the drupal update report:
`$settings['dept_update.is_prod'] = true;`
5. Now is the dept_update endpoint ready, you can get the update information in json format by:`/api/updates/<YOUR_SOME_REALLY_RAMDOM_TOKEN>`
